function assinar() {
    let botao = document.getElementById('botao_seInscreva');
    let mensagem = "Você já fez sua inscrição";

    if(botao.innerHTML === "Quero me inscrever agora"){
        botao.innerHTML = "Inscrito com sucesso";
        mensagem = "Sua inscrição foi feita com sucesso! Agora aguarde que enviaremos uma mensagem para o seu email";
    }
    
    alert(mensagem);
}

function checarFormulario(){
    let nome = document.getElementById("nome").value;
    let email = document.getElementById("email").value;
    let cpf = document.getElementById("cpf").value;

    if(nome === "" || email === "" || cpf === ""){
        alert("Preencha todos os campos!");
    } else {
        assinar();
    }

}