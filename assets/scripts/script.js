function tabelaDeMultiplicacao(n){
    let tabela = [];

    for(let i = 0; i <= 10; i++){
        tabela[i] = [];
        for(let j = 0; j <= 10; j++){
            tabela[i].push(j*i);
        }
    }
    
    return tabela;
}

console.table(tabelaDeMultiplicacao(10));
